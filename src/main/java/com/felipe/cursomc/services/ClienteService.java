package com.felipe.cursomc.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.felipe.cursomc.domain.Cliente;
import com.felipe.cursomc.repository.ClienteRepository;
import com.felipe.cursomc.services.exception.ObjectNotFoundException;

@Service
public class ClienteService{
	
	@Autowired
	private ClienteRepository repository;
	
	public Cliente find(Integer id) {
		Optional<Cliente> obj = repository.findById(id);
		
		/* Deprecated
		 *********************************************************************************
		if(obj == null) {
			throw new ObjectNotFoundException();
		}
		
		return obj.orElse(null);
		**********************************************************************************
		*/
		
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName()));
	}
}
