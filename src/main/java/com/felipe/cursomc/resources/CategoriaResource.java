package com.felipe.cursomc.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.felipe.cursomc.domain.Categoria;
import com.felipe.cursomc.services.CategoriaService;

/**
 * 
 * @author Felip
 *
 */
@RestController
@RequestMapping(value="/categorias")
public class CategoriaResource {
	
	@Autowired
	private CategoriaService service;
	
	@RequestMapping( value="/{id}", method=RequestMethod.GET)
//	@GetMapping
	public ResponseEntity<?> find(@PathVariable Integer id) {
		
		/* 
		 * Controladores REST costuma ter métodos pequenos
		 * Tratamento try-catch pode alongar muito o código
		 *  Alternativa: Handler!
		try {
		Categoria obj = service.find(id);
		return ResponseEntity.ok().body(obj);
		}catch() {
		}
		 */
		Categoria obj = service.find(id);
		return ResponseEntity.ok().body(obj);
		
//		Categoria categoria1 = new Categoria(1, "Informática");
//		Categoria categoria2 = new Categoria(2, "Escritório");
//		
//		/*
//		 * List é uma interface, e não pode ser instanciado.
//		 * Portanto, é preciso de uma classe que implemente essa interface,
//		 *  para então instanciar a interface List.
//		 */
//		List<Categoria> categorias = new ArrayList<>();
//		categorias.add(categoria1);
//		categorias.add(categoria2);
//		
//		return categorias;
	}
}
